package com.softserve.edu.numbersequence;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class NumberSequenceGenerator {

    private static void validateArguments(int n) {
        if(n < 0) {
            throw new IllegalArgumentException("n must be >= 0");
        }
    }

    public static int[] sequence(int n) {
        validateArguments(n);
        int size = (int)(Math.ceil(Math.sqrt(n)));
        return IntStream.range(0, size).toArray();
    }

}
