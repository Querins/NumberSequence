package com.softserve.edu.numbersequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        switch(args.length) {
            case 0:
                printInstructions();
                break;
            case 1:
                try {
                    int n = Integer.parseInt(args[0]);
                    System.out.println(Arrays.toString(NumberSequenceGenerator.sequence(n)));
                } catch(IllegalArgumentException e) {
                    System.out.println("Bad n param");
                }
        }

    }

    private static void printInstructions() {

        System.out.println("Prints number sequence, so that each a in sequence is a^2 < n. n is 1st param");

    }

}
