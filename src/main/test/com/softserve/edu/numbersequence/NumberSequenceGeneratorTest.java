package com.softserve.edu.numbersequence;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumberSequenceGeneratorTest {

    @Test(expected = IllegalArgumentException.class)
    public void sequence_minus_1() {

         NumberSequenceGenerator.sequence(-1);

    }


    @Test
    public void sequence_0() {

        assertArrayEquals(new int[] { }, NumberSequenceGenerator.sequence(0));

    }

    @Test
    public void sequence_36() {

        assertArrayEquals(new int[] { 0,1,2,3,4,5 }, NumberSequenceGenerator.sequence(36));

    }

    @Test
    public void sequence_88() {

        assertArrayEquals(new int[] { 0,1,2,3,4,5,6,7,8,9 }, NumberSequenceGenerator.sequence(88));

    }

    @Test
    public void sequence_122() {

        assertArrayEquals(new int[] { 0,1,2,3,4,5,6,7,8,9,10,11 }, NumberSequenceGenerator.sequence(122));

    }

}